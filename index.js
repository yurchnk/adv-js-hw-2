const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
];

let idRoot = document.getElementById('root');

class MyError extends Error {
    constructor(message, options) {
      super(message);
    }
  }
class Book{
    constructor(author, name, price){
        this.author = author;
        this.name = name;
        this.price = price;
    }
    get author(){
        return this._author;
    }
    set author(author){
        try{
            if(author == undefined)
            {
                throw UndefValue;
            }
            else{
                this._author = author;
            }
        }
        catch(UndefValue){
            console.error(new MyError('Set Author'));
        }
    }

    get name(){
        return this._name;
    }
    set name(name){
        try{
            if(name == undefined)
            {
                throw UndefValue;
            }
            else{
                this._name = name;
            }
        }
        catch(UndefValue){
            console.error(new MyError('Set Name'));
        }
    }

    get price(){
        return this._price;
    }
    set price(price){
        try{
            if(price == undefined)
            {
                throw UndefValue;
            }
            else{
                this._price = price;
            }
        }
        catch(UndefValue){
            console.error(new MyError('Set Price'));
        }
    }
}
for (const key in books) {
    let ul = document.createElement('ul');
    let book = new Book(books[key].author, books[key].name, books[key].price);
    if(books[key].author != undefined){
        ul.appendChild(document.createElement('li')).textContent = book.author;
    }
    if(books[key].name != undefined){
        ul.appendChild(document.createElement('li')).textContent = book.name;
    }
    if(books[key].price != undefined){
        ul.appendChild(document.createElement('li')).textContent = book.price;
    }
    idRoot.appendChild(ul);
}